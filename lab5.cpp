
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"

using namespace std;

enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };
typedef struct Card {
	Suit suit;
	int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);

bool suit_order(const Card& lhs, const Card& rhs);

int myrandom (int i) { return std::rand()%i;}

int main(int argc, char const *argv[]) {
	// IMPLEMENT as instructed below
	/*This is to seed the random generator */
	srand(unsigned (time(0)));



	/*Create a deck of cards of size 52 (hint this should be an array) and
	*  initialize the deck*/
	/*After the deck is created and initialzed we call random_shuffle() see 
	the
	*  notes to determine the parameters to pass in.*/
	/*Build a hand of 5 cards from the first five cards of the deck created
	*  above*/
	/*Sort the cards.  Links to how to call this function is in the specs
	*  provided*/
	/*Now print the hand below. You will use the functions get_card_name 
	and
	*  get_suit_code */
	
	
	
	Card deck[52]; //creates deck
	int num = 0;


	
	for (int i = 0; i < 4; i++){
		for (int j = 14; j >= 2; j--){
			
			//cout << "test" << num << endl;
			deck[num].value = j;
			deck[num].suit = static_cast<Suit>(i);
			num++;
		}
	}
	


	random_shuffle(&deck[0], &deck[52], myrandom); //shuffles deck
	

	Card hand[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};//creates hand

	for (Card c: hand){
		cout << get_card_name(c) << get_suit_code(c) << endl;

	}

	for (int i = 0; i < 4; i++){ //sorts by value of suit code
		if ((suit_order(hand[i],hand[i+1]) == false)){
		
			Card temp = hand[i];
			hand[i] = hand[i+1];
			hand[i+1] = temp;
		}
	}

	for (Card c: hand){
		//cout << get_card_name(c) << get_suit_code(c) << endl;
		cout << get_card_name(c) << c.value << endl;
	}
	cout << endl;

	
	return 0;
}
/*This function will be passed to the sort funtion. Hints on how to 
implement
*  this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
	
//	string left = get_suit_code(lhs);
//	string right = get_suit_code(rhs);


	if (lhs.suit < rhs.suit){ //calls get_suit_code and compares
		return true;
	}
	else if(lhs.suit == rhs.suit){
		if (lhs.value > rhs.value){
			return true;
		}
		else{
			return false;
		}
	}
	else{
		return false;
	}
}
string get_suit_code(Card& c) {
	switch (c.suit) {
		case SPADES:    return "\u2660";
		case HEARTS:    return "\u2661";
		case DIAMONDS:  return "\u2662";
		case CLUBS:     return "\u2663";
		default:        return "";
	}
}

string get_card_name(Card& c) {
	// IMPLEMENT
	
	string name;

	switch (c.value){
		case 11:
			name = "JACK";
			return name;

		case 12:
			name = "QUEEN";
			return name;

		case 13:
			name = "KING";
			return name;

		case 14:
			name = "ACE";
			return name;

		default:
			name = to_string(c.value);
			return name;
	}	


	


}


